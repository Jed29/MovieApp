import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.page.html',
  styleUrls: ['./movie-detail.page.scss'],
})
export class MovieDetailPage implements OnInit {

  detailMovieTab: string = "episode";

  constructor(private router: Router) { }

  ngOnInit() {
  }

  toHome(){
    this.router.navigate(['tabs/home']);
  }
}
